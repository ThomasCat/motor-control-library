/*
  MIT License

  Copyright (c) 2020 Owais Shaikh

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

// MotorControl Tester

#include "MotorControl.h"
#define BAUDRATE 2400

void setup(){

}

void loop(){
  Serial.begin(BAUDRATE);
  Serial.println("\n____________________________\n<< MotorControl Tester >>\n");
  Serial.print("> start / stop? : ");
  while(!Serial.available()); Serial.flush(); String addorremove=Serial.readString();addorremove.trim();Serial.print(addorremove);
  Serial.print("\n  + Enter pin number : ");
  while(!Serial.available()); int pin=Serial.parseInt(); Serial.print(pin);
  MotorControl mc = MotorControl(pin);
  if (addorremove.equals("start")){
    Serial.print("\n  + Enter speed : ");
    while(!Serial.available()); int speed=Serial.parseInt(); Serial.print(speed);
    mc.controlMotor(speed);
    Serial.print("\n[ ! ] Motor on pin "); Serial.print(pin); Serial.print(" is ON.\n");
  } else {
    Serial.print("\n[ X ] Motor on pin "); Serial.print(pin); Serial.print(" is OFF.\n");
    mc.stopMotor();
  }
}